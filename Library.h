#pragma once
#include <iostream>
#include <cmath>

namespace fun
{
	template<typename T, int N, int M>
	struct MasWrapper
	{
		T mas[N][M];
	};

	template<typename T, int N, int M>
	class Matrix
	{
	public:

		int m_n, m_m;
		T m_mat[N][M];

		Matrix()
		{

			m_n = N;
			m_m = M;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = 0;
		}

		Matrix(const T mas[N][M])
		{
			m_n = N;
			m_m = M;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mas[i][j];
		}

		Matrix(const MasWrapper<T, N, M>& mas)
		{

			m_n = N;
			m_m = M;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mas.mas[i][j];
		}

		Matrix(const Matrix<T, N, M>& mat)
		{

			m_n = mat.m_n;
			m_m = mat.m_m;

			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mat.m_mat[i][j];
		}

		int getN() const { return m_n; }
		int getM() const { return m_m; }
		T get(int i, int j) const { return m_mat[i][j]; }
		void set(int i, int j, T data) { m_mat[i][j] = data; }

		template<typename T, int N, int M>
		Matrix<T, N, M>& operator=(const Matrix<T, N, M>& mat)
		{

			m_n = mat.getN();
			m_m = mat.getM();

			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					m_mat[i][j] = mat.get(i, j);

			return *this;
		}

		template<typename T, int N, int M>
		Matrix<T, N, M> operator+(const Matrix<T, N, M>& mat)
		{

			Matrix<T, N, M> tmp;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					tmp.m_mat[i][j] = m_mat[i][j] + mat.m_mat[i][j];
			return tmp;
		}

		template<typename T, int N, int M>
		Matrix<T, N, M> operator-(const Matrix<T, N, M>& mat)
		{

			Matrix<T, N, M> tmp;
			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
					tmp.m_mat[i][j] = m_mat[i][j] - mat.m_mat[i][j];
			return tmp;
		}

		template<typename T, int N, int M>
		Matrix<T, N, M> operator*(const Matrix<T, N, M>& mat)
		{

			Matrix<T, N, M> tmp;

			for (int i = 0; i < m_n; i++)
				for (int j = 0; j < m_m; j++)
				{
					T sum = 0;
					for (int k = 0; k < m_m; k++)
						sum += m_mat[i][k] * mat.get(k, j);
					tmp.set(i, j, sum);

				}

			return tmp;
		}

		Matrix<T, N, M> operator* (T& k)
		{

			Matrix<T, N, M> tmp;

			for (int i = 0; i < m_n; i++)
			{
				for (int j = 0; j < m_m; j++)
				{
					tmp.m_mat[i][j] = m_mat[i][j] * k;
				}
			}

			return tmp;

		}

		Matrix<T, N - 1, M - 1> minor(int i, int j)
		{
			Matrix<T, N - 1, M - 1> result;

			int k;
			int l;

			for (int f = 0; f < m_n; f++)
			{
				for (int g = 0; g < m_m; g++)
				{
					if (f < i)
						k = f;
					else if (f > i)
						k = f - 1;
					else continue;
					if (g < j)
						l = g;
					else if (g > j)
						l = g - 1;
					else continue;
					result.m_mat[k][l] = m_mat[f][g];
				}
			}
			return result;
		}

		Matrix<T, N - 1, M - 1> alg(int i, int j)
		{
			Matrix<T, N - 1, M - 1> result;
			Matrix<T, N - 1, M - 1> a;

			a = this->minor(i, j);

			int k;
			int l;

			for (int f = 0; f < m_n; f++)
			{
				for (int g = 0; g < m_m; g++)
				{
					if (f < i)
						k = f;
					else if (f > i)
						k = f - 1;
					else continue;
					if (g < j)
						l = g;
					else if (g > j)
						l = g - 1;
					else continue;
					result.m_mat[k][l] = pow(-1, i + j) * m_mat[f][g];
				}
			}

			return result;
		}

		T det()
		{
			Matrix<T, N - 1, M - 1> v;
			T result = 0;
			T d = 0;
			T r = 0;

			if (m_n == 1 || m_m == 1)
			{
				result = m_mat[0][0];
				return result;
			}

			if (m_n == 2 || m_m == 2)
			{
				result = (m_mat[0][0] * m_mat[1][1]) - (m_mat[1][0] * m_mat[0][1]);
				return result;
			}

			if (m_n >= 3 || m_m >= 3)
			{
				int q = 0;
				for (int s = 0; s < m_m; s++)
				{
					v = this->alg(q,s);
					result += v.det() * this->m_mat[q][s];
				}
				/*result = m_mat[0][0] * m_mat[1][1] * m_mat[2][2] + m_mat[0][1] * m_mat[1][2] * m_mat[2][0]
					+ m_mat[1][0] * m_mat[2][1] * m_mat[0][2] - m_mat[2][0] * m_mat[1][1] * m_mat[0][2] -
					m_mat[2][1] * m_mat[1][2] * m_mat[0][0] - m_mat[1][0] * m_mat[0][1] * m_mat[2][2];*/
				return result;
			}

			/*if (m_n >= 4 || m_m >= 4)
			{
				std::cout << "Invalid opiration" << std::endl;
			}*/
		}

		T slau(Matrix<T, N, 1>& sv)
		{
			Matrix<T, N, M> X;
			Matrix<T, N, M> Y;
			T X_0;
			T Otvet;

			X_0 = this->det();

			for (int i = 0; i < m_n; i++)
			{
				for (int j = 0; j < m_m; j++)
				{
					X.m_mat[i][j] = this->m_mat[i][j];
					Y.m_mat[i][j] = this->m_mat[i][j];
				}
			}

			for (int j = 0; j < m_n; j++)
			{
				for (int i = 0; i < m_m; i++)
				{
					X.m_mat[i][j] = sv.m_mat[i][0];
				}
				Otvet = X.det() / X_0;
				std::cout << Otvet << " ";

				for (int i = 0; i < m_m; i++)
					X.m_mat[i][j] = Y.m_mat[i][j];
			}
			return 0;
		}

		Matrix<T, N, M> inv()
		{
			Matrix<T, N, M> tmp;
			Matrix<T, N - 1, M - 1> tmp_1;

			double d = det();

			if (d == 0)
				throw std::exception("nonzero determinant!");
			else
			{
				if (m_n == 2 || m_m == 2)
				{
					tmp.m_mat[0][0] = m_mat[1][1] / d;
					tmp.m_mat[0][1] = -m_mat[0][1] / d;
					tmp.m_mat[1][0] = -m_mat[1][0] / d;
					tmp.m_mat[1][1] = m_mat[0][0] / d;

					return tmp;
				}
				else if (m_n == 3 || m_m == 3)
				{
					tmp.m_mat[0][0] = (m_mat[1][1] * m_mat[2][2] - m_mat[2][1] * m_mat[1][2]) / d;
					tmp.m_mat[1][0] = -(m_mat[1][0] * m_mat[2][2] - m_mat[2][0] * m_mat[1][2]) / d;
					tmp.m_mat[2][0] = (m_mat[1][0] * m_mat[2][1] - m_mat[2][0] * m_mat[1][1]) / d;
					tmp.m_mat[0][1] = -(m_mat[0][1] * m_mat[2][2] - m_mat[2][1] * m_mat[0][2]) / d;
					tmp.m_mat[1][1] = (m_mat[0][0] * m_mat[2][2] - m_mat[2][0] * m_mat[0][2]) / d;
					tmp.m_mat[2][1] = -(m_mat[0][0] * m_mat[2][1] - m_mat[2][0] * m_mat[0][1]) / d;
					tmp.m_mat[0][2] = (m_mat[0][1] * m_mat[1][2] - m_mat[1][1] * m_mat[0][2]) / d;
					tmp.m_mat[1][2] = -(m_mat[0][0] * m_mat[1][2] - m_mat[1][0] * m_mat[0][2]) / d;
					tmp.m_mat[2][2] = (m_mat[0][0] * m_mat[1][1] - m_mat[1][0] * m_mat[0][1]) / d;

					return tmp;
				}
				/*for (int j = 0; j < m_n; j++)
				{
					for (int i = 0; i < m_m; i++)
					{
						tmp_1 = tmp.alg(i, j);
						tmp.m_mat[i][j] = tmp_1.det();
					}
				}
				tmp = tmp.tran();
				tmp = tmp * (1 / (this->det()));

				return tmp;*/
			}

			std::cout << "After throw exception in inv function!" << std::endl;

			return tmp;
		}

		Matrix<T, N, M> tran()
		{
			Matrix<T, N, M> tmp;

			for (int i = 0; i < m_n; i++)
			{
				for (int j = 0; j < m_m; j++)
				{
					tmp.m_mat[i][j] = m_mat[j][i];
				}
			}
			return tmp;
		}

		~Matrix()
		{

		}

		template<typename T, int N, int M>
		friend std::istream& operator>>(std::istream& os, Matrix<T, N, M>& mat);

		template<typename T, int N, int M>
		friend std::ostream& operator<<(std::ostream& os, const Matrix<T, N, M>& mat);

	};

	template<typename T, int N, int M>
	std::istream& operator>>(std::istream& in, Matrix<T, N, M>& mat)
	{
		for (int i = 0; i < mat.m_n; i++)
			for (int j = 0; j < mat.m_m; j++)
				in >> mat.m_mat[i][j];
		return in;
	}

	template<typename T, int N, int M>
	std::ostream& operator<<(std::ostream& out, const Matrix<T, N, M>& mat)
	{
		for (int i = 0; i < mat.m_n; i++) {
			for (int j = 0; j < mat.m_m; j++)
				out << mat.m_mat[i][j] << " ";
			out << std::endl;
		}
		return out;
	}


	using Vec3d = Matrix<double, 3, 1>;
	using Mat33d = Matrix<double, 3, 3>;
}
